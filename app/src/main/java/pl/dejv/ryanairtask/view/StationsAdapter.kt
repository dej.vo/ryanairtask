package pl.dejv.ryanairtask.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import pl.dejv.ryanairtask.model.Station

class StationsAdapter(context: Context, private val layoutId: Int, private var data: ArrayList<Station>) : ArrayAdapter<Station>(context, layoutId, data) {

		private var suggestions: ArrayList<Station> = arrayListOf()
		private var tempData = ArrayList<Station>(data)
		var selectedItem: Station? = null

		override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
				var view = convertView
				if (convertView == null) {
						view = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1, parent, false)
				}

				val stationTV = view!!.findViewById<TextView>(android.R.id.text1)
				stationTV.text = getItem(position)!!.name

				return view
		}

		override fun getItem(position: Int): Station? {
				return super.getItem(position)
		}

		override fun getCount(): Int {
				return super.getCount()
		}

		override fun getFilter(): Filter {
				return stationsFilter
		}

		fun setData(stations: ArrayList<Station>) {
				data = stations
				tempData = stations
				notifyDataSetChanged()
		}

		fun setSelectedItem(position: Int) {
				selectedItem = getItem(position)
		}

		private val stationsFilter = object : Filter() {

				override fun convertResultToString(resultValue: Any?): CharSequence {
						return (resultValue as Station).name
				}

				override fun performFiltering(chars: CharSequence?): FilterResults {
						val result = FilterResults()
						if (chars != null) {
								suggestions.clear()
								for (station in tempData) {
										if (station.name.toLowerCase().startsWith(chars.toString(), true)) {
												suggestions.add(station)
										}
								}
								result.values = suggestions
								result.count = suggestions.size
						} else {
								result.values = tempData
								result.count = tempData.size
						}

						return result
				}

				override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
						if (results != null && results.count > 0) {
								clear()
								val data = results.values as ArrayList<*>
								for (item in data) {
										add(item as Station)
										notifyDataSetChanged()
								}
						} else {
								clear()
								notifyDataSetChanged()
						}
				}

		}
}