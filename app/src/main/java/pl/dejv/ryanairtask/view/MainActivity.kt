package pl.dejv.ryanairtask.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*
import pl.dejv.ryanairtask.R
import pl.dejv.ryanairtask.viewModel.RyanairViewModel

class MainActivity : AppCompatActivity() {

		private val viewModel by lazy { ViewModelProviders.of(this).get(RyanairViewModel::class.java) }

		override fun onCreate(savedInstanceState: Bundle?) {
				super.onCreate(savedInstanceState)
				setContentView(R.layout.activity_main)

				setSupportActionBar(toolbar)

				supportFragmentManager
								.beginTransaction()
								.add(R.id.fragment_container, SearchFlightsFragment.getInstance())
								.commit()

				setupGetStationButton()
		}

		private fun setupGetStationButton() {
				viewModel.getStationsLive()
		}
}
