package pl.dejv.ryanairtask.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.flight_results_layout.*
import kotlinx.android.synthetic.main.toolbar_flight_results.*
import pl.dejv.ryanairtask.R
import pl.dejv.ryanairtask.model.Date
import pl.dejv.ryanairtask.model.FlightDetailsItem
import pl.dejv.ryanairtask.model.FlightResultsResponse
import pl.dejv.ryanairtask.viewModel.RyanairViewModel

class FlightResultsFragment : Fragment() {

		private lateinit var adapter: FlightsResultsAdapter
		private var viewModel: RyanairViewModel? = null
		private var priceFilterTop: Int? = null

		companion object {
				fun getInstance() = FlightResultsFragment()
		}

		override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
				return inflater.inflate(R.layout.flight_results_layout, container, false)
		}

		override fun onActivityCreated(savedInstanceState: Bundle?) {
				super.onActivityCreated(savedInstanceState)
				arguments?.let {
						setupViews()
				}
				setupViewModel()
				setupViewListeners()
		}

		private fun setupViewModel() {
				viewModel = ViewModelProviders.of(activity!!).get(RyanairViewModel::class.java)
				viewModel!!.flightsLiveData.value?.let {
						val flightResults = FlightResults(it)
						adapter = FlightsResultsAdapter(
										context!!,
										flightResults,
										priceFilterTop!!,
										object : FlightResultOnClickListener<FlightDetailsItem> {
												override fun onItemClick(item: FlightDetailsItem) {
														val fragment = FlightDetailsFragment.getInstance()
														fragment.setData(item)
														fragmentManager!!.beginTransaction()
																		.hide(this@FlightResultsFragment)
																		.add(R.id.fragment_container, fragment)
																		.addToBackStack(null)
																		.commit()
												}
										})

						flightResultsRV.adapter = adapter
						flightResultsRV.layoutManager = LinearLayoutManager(context)
				}
		}

		private fun setupViews() {
				val origin = arguments!!.getString(SearchFlightsFragment.STATION_ORIGIN)
				val destination = arguments!!.getString(SearchFlightsFragment.STATION_DESTINATION)
				priceFilterTop = arguments!!.getInt(SearchFlightsFragment.PRICE_TOP_FILTER)
				activity!!.stationOriginSubtitle.text = origin
				activity!!.stationDestinationSubtitle.text = destination
				activity!!.stationOriginTitle.text = getString(R.string.station_origin)
				activity!!.stationDestinationTitle.text = getString(R.string.station_destination)
				priceSlider.progress = priceFilterTop as Int
				priceValue.text = priceFilterTop.toString()
		}

		private fun setupViewListeners() {
				setupPriceSliderListener()
		}

		private fun setupPriceSliderListener() {
				priceSlider.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
						override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
								priceValue.text = progress.toString()
								// update recyclerView underlying data
								adapter.updateFilter(progress)
						}

						override fun onStartTrackingTouch(seekBar: SeekBar?) {
						}

						override fun onStopTrackingTouch(seekBar: SeekBar?) {
						}

				})
		}
}

data class FlightResults(private val response: FlightResultsResponse) {
		val currency: String = response.currency
		val origin: String = response.trips[0].originName
		val destination: String = response.trips[0].destinationName
		val dates: ArrayList<Date> = response.trips[0].dates
		val flights = dates.flatMap { date -> date.flights }

}
