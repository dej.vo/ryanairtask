package pl.dejv.ryanairtask.view

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.InputType
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.price_filter_slider.*
import kotlinx.android.synthetic.main.search_flight_layout.*
import kotlinx.android.synthetic.main.toolbar_flight_results.*
import pl.dejv.ryanairtask.R
import pl.dejv.ryanairtask.model.FlightSearchItem
import pl.dejv.ryanairtask.viewModel.FlightStatus
import pl.dejv.ryanairtask.viewModel.RyanairViewModel
import java.util.*

class SearchFlightsFragment : Fragment() {

		private var ryanairViewModel: RyanairViewModel? = null
		private var originAdapter: StationsAdapter? = null
		private var destinationAdapter: StationsAdapter? = null

		companion object {
				fun getInstance() = SearchFlightsFragment()
				const val DATA_FORMAT: String = "yyyy-MM-dd"
				const val STATION_ORIGIN: String = "station.origin"
				const val STATION_DESTINATION: String = "station.destination"
				const val PRICE_TOP_FILTER: String = "price.top.filter"
		}

		override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
				return inflater.inflate(R.layout.search_flight_layout, container, false)
		}

		override fun onHiddenChanged(hidden: Boolean) {
				super.onHiddenChanged(hidden)
				resetViews()
		}

		override fun onActivityCreated(savedInstanceState: Bundle?) {
				super.onActivityCreated(savedInstanceState)
				setupViewModel()
				setupOriginStation()
				setupDestinationStation()
				setupListeners()
		}

		private fun setupListeners() {
				setupOriginStationListener()
				setupDestinationStationListener()
				setupDepartureDateListener()
				setupPriceFilterListener()

				// search
				search.setOnClickListener {
						// TODO : validate all input fields

						val flightSearchItem = FlightSearchItem(
										originAdapter?.selectedItem?.code ?: EMPTY_STRING,
										destinationAdapter?.selectedItem?.code ?: EMPTY_STRING,
										departureDate.text.toString(),
										1)
						ryanairViewModel?.searchFlights(flightSearchItem)
				}

		}

		private fun setupPriceFilterListener() {
				priceValue.text = getString(R.string.price_filter_default)
				priceSlider.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
						override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
								priceValue.text = progress.toString()
						}

						override fun onStartTrackingTouch(seekBar: SeekBar?) {
						}

						override fun onStopTrackingTouch(seekBar: SeekBar?) {
						}

				})
		}

		private fun setupViewModel() {
				ryanairViewModel = ViewModelProviders.of(activity!!).get(RyanairViewModel::class.java)
				ryanairViewModel!!.liveData.observe(this, Observer {
						originAdapter?.setData(it.stations)
						destinationAdapter?.setData(it.stations)
				})

				ryanairViewModel!!.state.observe(this, Observer {
						when (it.status) {
								FlightStatus.SEARCHING -> {
										// launch some progress indicator
								}

								FlightStatus.SUCCESS -> {
										// launch flight results fragment
										launchFlightResultsView()
								}

								FlightStatus.ERROR -> {
										// show error message
								}
						}
				})
		}

		private fun launchFlightResultsView() {
				val fragment = FlightResultsFragment.getInstance()
				fragment.arguments = Bundle()
				fragment.arguments!!
								.putString(STATION_ORIGIN, originAdapter!!.selectedItem!!.name)
				fragment.arguments!!
								.putString(STATION_DESTINATION, destinationAdapter!!.selectedItem!!.name)
				fragment.arguments!!
								.putInt(PRICE_TOP_FILTER, priceSlider.progress)

				fragmentManager!!
								.beginTransaction()
								.hide(this)
								.add(R.id.fragment_container, fragment)
								.addToBackStack(null)
								.commit()
		}

		private fun setupOriginStation() {
				originStation.threshold = 0
				originAdapter = StationsAdapter(context!!, 0, ryanairViewModel?.liveData?.value?.stations ?: arrayListOf())
				originStation.setAdapter(originAdapter)
		}

		private fun setupDestinationStation() {
				destinationStation.threshold = 0
				destinationAdapter = StationsAdapter(context!!, 0, ryanairViewModel?.liveData?.value?.stations
								?: arrayListOf())
				destinationStation.setAdapter(destinationAdapter)
		}

		private fun setupOriginStationListener() {
				originStation.setOnClickListener { originStation.showDropDown() }
				originStation.setOnItemClickListener { parent, view, position, id ->
						originAdapter!!.setSelectedItem(position)
				}
		}

		private fun setupDestinationStationListener() {
				destinationStation.setOnClickListener { destinationStation.showDropDown() }
				destinationStation.setOnItemClickListener { parent, view, position, id ->
						destinationAdapter!!.setSelectedItem(position)
				}
		}

		private fun setupDepartureDateListener() {
				departureDate.inputType = InputType.TYPE_NULL

				val cal = Calendar.getInstance()
				val year = cal.get(Calendar.YEAR)
				val month = cal.get(Calendar.MONTH)
				val day = cal.get(Calendar.DAY_OF_MONTH)

				departureDate.setOnClickListener {
						DatePickerDialog(context!!,
										DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
												typeDestinationDate(year, month, dayOfMonth)
										}, year, month, day)
										.show()
				}
		}

		private fun typeDestinationDate(year: Int, month: Int, dayOfMonth: Int) {
				departureDate.setText(EMPTY_STRING)
				val formattedData = DateFormat.format(DATA_FORMAT, GregorianCalendar(year, month, dayOfMonth))
				departureDate.setText(formattedData)
		}

		private fun resetViews() {
				activity!!.stationOriginTitle.text = EMPTY_STRING
				activity!!.stationDestinationTitle.text = EMPTY_STRING
				activity!!.stationOriginSubtitle.text = EMPTY_STRING
				activity!!.stationDestinationSubtitle.text = EMPTY_STRING
		}
}