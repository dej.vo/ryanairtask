package pl.dejv.ryanairtask.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import pl.dejv.ryanairtask.model.FlightResultsResponse
import pl.dejv.ryanairtask.model.FlightSearchItem
import pl.dejv.ryanairtask.model.StationsResponse
import pl.dejv.ryanairtask.repository.FlightRepository
import pl.dejv.ryanairtask.repository.StationsRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RyanairViewModel : ViewModel() {

		var liveData: MutableLiveData<StationsResponse> = MutableLiveData()
		var flightsLiveData: MutableLiveData<FlightResultsResponse> = MutableLiveData()
		val state: MutableLiveData<FlightState> = MutableLiveData()

		private var result: StationsResponse? = null

		companion object {
				private val stationsRepository: StationsRepository = StationsRepository()
				private val flightRepository = FlightRepository()
		}

		fun getStationsLive() {
				if (result != null) {
						liveData.postValue(result)
				} else {
						stationsRepository.getStations(object : Callback<StationsResponse> {
								override fun onFailure(call: Call<StationsResponse>, t: Throwable) {

								}

								override fun onResponse(call: Call<StationsResponse>, response: Response<StationsResponse>) {
										if (response.isSuccessful) {
												result = response.body()
												liveData.postValue(result)
										}
								}
						})
				}
		}

		fun searchFlights(search: FlightSearchItem) {
				state.postValue(FlightState.searching())
				flightRepository.searchFlights(search, object : Callback<FlightResultsResponse> {
						override fun onFailure(call: Call<FlightResultsResponse>, t: Throwable) {
								state.postValue(FlightState.error(t))
						}

						override fun onResponse(call: Call<FlightResultsResponse>, response: Response<FlightResultsResponse>) {
								if (response.isSuccessful) {
										flightsLiveData.postValue(response.body())
										state.postValue(FlightState.success())
								} else {
										state.postValue(FlightState.unsuccessful())
								}
						}

				})
		}
}