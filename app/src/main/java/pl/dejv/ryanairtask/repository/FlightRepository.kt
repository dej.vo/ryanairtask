package pl.dejv.ryanairtask.repository

import pl.dejv.ryanairtask.api.RyanairApi
import pl.dejv.ryanairtask.model.FlightResultsResponse
import pl.dejv.ryanairtask.model.FlightSearchItem
import pl.dejv.ryanairtask.socket.ApiSocket
import retrofit2.Callback

class FlightRepository {

		companion object {
				val api = ApiSocket.INSTANCE
		}

		fun searchFlights(searchItem: FlightSearchItem, callback: Callback<FlightResultsResponse>) {
				api.create(RyanairApi::class.java)
								.search(origin = searchItem.origin,
												destination = searchItem.destination,
												dateOut = searchItem.dateOut,
												adult = searchItem.adult)
								.enqueue(callback)
		}
}
