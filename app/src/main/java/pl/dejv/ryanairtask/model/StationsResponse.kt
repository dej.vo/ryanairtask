package pl.dejv.ryanairtask.model

data class StationsResponse(val stations: ArrayList<Station>)

data class Station(val code: String, val name: String, val alternateName: String)
