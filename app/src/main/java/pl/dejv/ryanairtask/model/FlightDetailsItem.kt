package pl.dejv.ryanairtask.model

data class FlightDetailsItem(val origin: String, val destination: String, val currency: String, val flight: Flight, val amount: Float?)