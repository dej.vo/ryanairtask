package pl.dejv.ryanairtask.model

data class FlightResultsResponse(val termsOfUse: String,
																 val currency: String,
																 val currPrecision: Int,
																 val trips: ArrayList<Trip>)

data class Trip(val origin: String,
								val originName: String,
								val destination: String,
								val destinationName: String,
								val dates: ArrayList<Date>)

data class Date(val dateOut: String,
								val flights: ArrayList<Flight>)

data class Flight(val faresLeft: Int,
									val flightKey: String,
									val infantsLeft: Int,
									val regularFare: RegularFare?,
									val flightNumber: String,
									val time: ArrayList<String>,
									val timeUTC: ArrayList<String>,
									val duration: String)


data class RegularFare(val fareKey: String,
											 val fareClass: String,
											 val fares: ArrayList<Fare>)

data class Fare(val type: String,
								val amount: Float,
								val count: Int,
								val hasDiscount: Boolean,
								val publishedFare: Float,
								val discountInPercent: Int,
								val hasPromoDiscount: Boolean)
