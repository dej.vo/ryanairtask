package pl.dejv.ryanairtask.model

data class FlightSearchItem(val origin: String,
														val destination: String,
														val dateOut: String,
														val adult: Int = 1,
														val teen: Int = 0,
														val child: Int = 0)
